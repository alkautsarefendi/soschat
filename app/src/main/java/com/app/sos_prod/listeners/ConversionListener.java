package com.app.sos_prod.listeners;

import com.app.sos_prod.models.User;

public interface ConversionListener {
    void onConversionClicked(User user);
}
